import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import BlockingForm from "./BlockingForm";

class PreventTransition extends Component {
  render() {
    return (
      <Router>
        <ul>
          <li>
            <Link to="/">Form</Link>
          </li>
          <li>
            <Link to="/one">One</Link>
          </li>
          <li>
            <Link to="/two">Two</Link>
          </li>
        </ul>

        <Switch>
          <Route path="/" exact children={<BlockingForm />} />
          <Route path="/one" children={<h3>One</h3>} />
          <Route path="/two" children={<h3>Two</h3>} />
        </Switch>
      </Router>
    );
  }
}

export default PreventTransition;
