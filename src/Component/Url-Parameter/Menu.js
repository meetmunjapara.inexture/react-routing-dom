import React, { Component } from "react";
import { Link } from "react-router-dom";

class Menu extends Component {
  render() {
    return (
      <div>
        <h1>Accounts</h1>
        <ul>
          <li>
            <Link to="/netflix">Netflix</Link>
          </li>
          <br />
          <li>
            <Link to="/yahoo">Yahoo</Link>
          </li>
          <br />
          <li>
            <Link to="/amazon">Amazon</Link>
          </li>
          <br />
          <li>
            <Link to="/flipkart">Flipkart</Link>
          </li>
        </ul>
      </div>
    );
  }
}
export default Menu;
