import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Account from "./Account";
import Menu from "./Menu";

class UrlPara extends Component {
  render() {
    return (
      <Router>
        <Menu />
        <hr />
        <Switch>
          <Route path="/:account">
            <Account />
          </Route>
        </Switch>
      </Router>
    );
  }
}
export default UrlPara;
