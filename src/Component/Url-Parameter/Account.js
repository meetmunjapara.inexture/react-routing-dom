import React from "react";
import { useParams } from "react-router-dom";
function Account() {
  let { account } = useParams();

  return <h3>ID: {account}</h3>;
}
export default Account;
