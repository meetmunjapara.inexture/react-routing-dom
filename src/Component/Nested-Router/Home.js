import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Home extends Component {
    render(){
        return(
            <div>
                <h2>
                    This is Home Page
                </h2>
                <NavLink to = '/'>Go Back</NavLink>
            </div>
        );
    }
}
export default Home;