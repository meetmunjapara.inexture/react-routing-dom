import React, { Component } from "react";
import { BrowserRouter, Switch, Route, Link, NavLink } from "react-router-dom";
import Home from "./Home";
import Topics from "./Topic";

class NestRout extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <ul>
            <li>
              <Link to="/Home">Home</Link>
            </li>
            <li>
              <Link to="/topics">Topics</Link>
            </li>
          </ul>

          <hr />

          <Switch>
            <Route exact path="/Home">
              <Home />
            </Route>
            <Route path="/topics">
              <Topics />
            </Route>
            <NavLink to="/">Go Back</NavLink>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default NestRout;
