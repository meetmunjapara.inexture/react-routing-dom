import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route ,Redirect} from "react-router-dom";
import Person from './Person';

class RecursivePath extends Component{
    render(){
        return(
            <Router>
            <Switch>
              <Route path="/:id">
                <Person />
              </Route>
              <Route path="/">
                <Redirect to="/0" />
              </Route>
            </Switch>
          </Router>
        );
    }
}
export default RecursivePath;