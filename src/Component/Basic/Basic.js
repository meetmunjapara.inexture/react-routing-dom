import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, NavLink } from "react-router-dom";
import About from "./About";
import Dashboard from "./Dashboard";
import Home from "./Home";
import Menu from "./Menu";


class Basic extends Component {
  render() {
    return (
      <Router>
        <Menu />
        <hr />
        <Switch>
          <Route exact path="/home">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          <NavLink to = '/'>Go Back</NavLink>
        </Switch>
      </Router> 
    );
  }
}
export default Basic;
