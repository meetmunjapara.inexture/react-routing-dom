import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Dashboard extends Component {
    render() {
        return (
          <div>
            <h1>Dashboard</h1>
            <p>This is Dashboard view.</p>
            <NavLink to = '/'>Go Back</NavLink>
          </div>
        );
      }
}

export default Dashboard;