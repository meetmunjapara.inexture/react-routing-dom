import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class About extends Component {
    render() {
        return (
          <div>
            <h1>About</h1>
            <p>This is About view.</p>
            <NavLink to = '/'>Go Back</NavLink>
          </div>
        );
      }
}

export default About;