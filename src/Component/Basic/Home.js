import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class Home extends Component {
  render() {
    return (
      <div>
        <h1>Home</h1>
        <p>This is Home view.</p>
        <NavLink to = '/'>Go Back</NavLink>
      </div>
    );
  }
}

export default Home;
