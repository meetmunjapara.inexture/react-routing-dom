import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class Error extends Component {
  render() {
    return (
      <div>
        <h1>404 Error Page</h1>
        <p>This page is doesn't valid.</p>
        <NavLink to="/">Go Back</NavLink>
      </div>
    );
  }
}

export default Error;
