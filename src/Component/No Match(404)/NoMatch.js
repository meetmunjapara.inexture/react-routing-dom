import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Menu from "./Menu";
import About from "./About";
import Dashboard from "./Dashboard";
import Home from "./Home";

class NoMatch extends Component {
  render() {
    return (
      <Router>
        <Menu />
        <hr />
        <Switch>
          <Route exact path="/home">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default NoMatch;
