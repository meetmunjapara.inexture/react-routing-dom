import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class MenuList extends Component {
    render(){
        return(
            <div className = 'menulist'>
                <h1> React Routing Example</h1>

                <Link to = '/basic'>Basic</Link>
                <br />
                <Link to = '/urlpara'>Url-ParaMeters</Link>
                <br />
                <Link to = '/nesting'>Nesting</Link>
                <br />
                <Link to = '/redirect'>Redirect(Auth)</Link>
                <br />
                <Link to = '/noMatch'>No Match(404)</Link>
                <br />
                <Link to = '/PreventTransition'>Prevent Transition</Link>
                <br />
                <Link to = '/RecursivePath'>Recursive Path</Link>
            </div>
        )
    }
}
export default MenuList;