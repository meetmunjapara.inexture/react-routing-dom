import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route ,Redirect} from "react-router-dom";
import "./App.css";
import Basic from "./Component/Basic/Basic";
import NestRout from "./Component/Nested-Router/NestRout";
import Error from "./Component/No Match(404)/Error";
import NoMatch from "./Component/No Match(404)/NoMatch";
import PreventTransition from "./Component/Preventing/PreventTransition";
import RecursivePath from "./Component/Recursive Path/RecursivePath";
import UrlPara from "./Component/Url-Parameter/UrlPara";
import MenuList from './MenuList';


class App extends Component {
  render() {
    return (
      
      <Router>
        <Switch>
          <Route exact path = '/'>
            <MenuList />
          </Route>
          <Route path="/basic">
            <Basic />
          </Route>
          <Route path="/urlpara">
            <UrlPara />
          </Route>
          <Route path="/nesting">
            <NestRout />
          </Route>
          <Route path="/noMatch">
            <NoMatch />
          </Route>
          <Route path = '/PreventTransition'>
            <PreventTransition />
          </Route>
          <Route path = '/RecursivePath'>
            <RecursivePath />
          </Route>
          <Route component = {Error}/>
          <Redirect to = '/' />
        </Switch>
      </Router>
      
    );
  }
}

export default App;
